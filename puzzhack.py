
import pygame
import pygame.locals
import random
import time


import constants
import utils
from block import Block
from blockham import Blockham
from pillar import Pillar
 
# Setup
pygame.init()
   
screen = pygame.display.set_mode([constants.SCREEN_WIDTH,constants.SCREEN_HEIGHT])
  
pygame.display.set_caption("Blockham's Sandpit")
  
#Loop until the user clicks the close button.
done = False
  
# Used to manage how fast the screen updates
clock = pygame.time.Clock()

#Score
score = 0
basicfont = pygame.font.SysFont('arial', 12)
biggerfont = pygame.font.SysFont('arial', 36)
scoretext = basicfont.render("Score: "+str(score),True,(16,16,16))
wintext = biggerfont.render("You Win!",True,(32,32,32))
win = False
#initialise player
blockham = Blockham()

#initialise blocks
pillars = [[Pillar(i,j,0) for j in range(10)] for i in range(10)]

#populate treasure
treasure = 5
xvals = random.sample(range(10),treasure)
yvals = random.sample(range(10),treasure)
dvals = random.sample(range(1,16),treasure)
treasurelist = []
treasurelist.append("Buried Feelings")
treasurelist.append("'Lost' Boxset")
treasurelist.append("Your Marbles")
treasurelist.append("Bitcoin Wallet")
treasurelist.append("Richard III")
treasurelist.append("Yourself")
treasurelist.append("More sand")
treasurelist.append("Meteorite")
treasurelist.append("Lifelike Parrot")
treasurelist.append("Contact Lenses")
treasurelist.append("Crystal Skull")
treasurelist.append("Ark of Covenant")
treasurelist.append("Holy Grail")
treasurelist.append("Horcrux")
treasurelist.append("Pod Racer")
treasurelist.append("Statue of Liberty")

randlist= random.sample(treasurelist,treasure)
for i in range(treasure):
    pillars[xvals[i]][yvals[i]].setTreasure(randlist[i],dvals[i])
    print(str(i)+pillars[xvals[i]][yvals[i]].getTreasureName())

star = pygame.image.load("star.bmp").convert()
star.set_colorkey(constants.MAGENTA)
#sound
beep = pygame.mixer.Sound("202530__kalisemorrison__scanner-beep.wav")
success = pygame.mixer.Sound("204369__philitup321__alert-sound.ogg")
win = pygame.mixer.Sound("169993__davidox__jaaaaa-hurraaaaa.ogg")
longcount = 0;
#movement
counter=0
xoffset=5
yoffset=5
#treasure finding
lastFind=""
lastfindx=-1
lastfindy=-1
lastfindz=-1
findstext=basicfont.render("Last Find: "+ lastFind,True,(32,32,32))

wasnear=False
d=-1

def checkNear():
    near = False
    global wasnear    
    x = blockham.getPosX()
    y=blockham.getPosY()
    z=blockham.getDepth()        
    for i in range(treasure):
        if utils.isNear((x,y,z),(xvals[i],yvals[i],dvals[i])):
            near=True
        if near:
            wasnear=True
        elif wasnear:
            wasnear=False
    return near

# -------- Main Program Loop -----------
while not done:
    if win == True:
        time.sleep(3)
        done=True        
    # ALL EVENT PROCESSING SHOULD GO BELOW THIS COMMENT   
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop 
        if event.type == pygame.KEYDOWN:              
            if event.key == pygame.K_SPACE:                    
                found=pillars[blockham.getPosX()][blockham.getPosY()].dig()
                if found != "No":
                    lastFind=found;
                    score = score+1
                    if score >= treasure:
                        win.play()   
                    else:
                        success.play()                    
                    findstext=basicfont.render("Last Find: "+ lastFind,True,(32,32,32))
                    scoretext = basicfont.render("Score: "+str(score),True,(16,16,16))
            
            elif event.key == pygame.K_UP:            
                blockham.start_up()
            elif event.key == pygame.K_DOWN:                
                blockham.start_down()
            elif event.key == pygame.K_LEFT:                
                blockham.start_left()
            elif event.key == pygame.K_RIGHT:                
                blockham.start_right()
    
                
                     
    # ALL EVENT PROCESSING SHOULD GO ABOVE THIS COMMENT
    
    # ALL GAME LOGIC SHOULD GO BELOW THIS COMMENT
    
   

    if blockham.getStatus() == 9:
        #we're digging
        counter = counter + 2
        if counter >= 20:
            blockham.finish_dig()
            counter = 0   
    elif blockham.getStatus() == 2:
        #going down
        counter = counter + 2
        yoffset = 5 + counter
        if counter >= 20:
            blockham.finish_down()
            counter =0
    elif blockham.getStatus() == 4:
        #going left
        counter = counter + 2
        xoffset = 5 - counter
        if counter >= 20:
            blockham.finish_left()
            counter = 0
    elif blockham.getStatus() == 6:
        #going right
        counter = counter + 2
        xoffset = 5 + counter
        if counter >= 20:
            blockham.finish_right()
            counter = 0
    elif blockham.getStatus() == 8:
        #going up
        counter = counter + 2
        yoffset = 5 - counter
        if counter >= 20:
            blockham.finish_up()
            counter = 0
    if counter == 0:
        longcount = longcount + 1
        xoffset = 5
        yoffset = 5
        blockham.setDepth(pillars[blockham.getPosX()][blockham.getPosY()].getDepth())
        out = checkNear()
        if out ==True:
            if longcount >=200:
                beep.play()
                longcount = 0
    blockham.update()
    # ALL GAME LOGIC SHOULD GO ABOVE THIS COMMENT
        
    # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
      
    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(constants.WHITE)   
    if score >= treasure:
        screen.blit(wintext,[50,50])
        win=True
        pygame.display.flip()
    else:
        for line in pillars:
            for pillar in line:
                pygame.draw.rect(screen,pillar.getColor(),[pillar.getPosX() * 20,pillar.getPosY() * 20,20,20])
                if pillar.getTreasureFound() == True:
                    screen.blit(star,[pillar.getPosX() * 20,pillar.getPosY()* 20])
                    
        screen.blit(blockham.image, [(blockham.getPosX() *20) + xoffset, (blockham.getPosY() * 20) + yoffset])
        screen.blit(scoretext,[5,205])
        screen.blit(findstext,[65,205])
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
          
        # Go ahead and update the screen with what we've drawn.
        pygame.display.flip()
      
    # Limit to 20 frames per second
    clock.tick(60)
      
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()