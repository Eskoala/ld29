# Import a library of functions called 'pygame'
import pygame
import math
# Initialize the game engine
pygame.init()
# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
size = (700, 500)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Claire's Cool Game")
#Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
# -------- Main Program Loop -----------
while not done:
    # ALL EVENT PROCESSING SHOULD GO BELOW THIS COMMENT
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop
    # ALL EVENT PROCESSING SHOULD GO ABOVE THIS COMMENT
 
 
    # ALL GAME LOGIC SHOULD GO BELOW THIS COMMENT
 
    # ALL GAME LOGIC SHOULD GO ABOVE THIS COMMENT
 
 
    # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
    # Clear the screen and set the screen background
    screen.fill(WHITE)
    for i in range(200):
     
        radians_x = i / 20
        radians_y = i / 6
     
        x = int( 75 * math.sin(radians_x)) + 200
        y = int( 75 * math.cos(radians_y)) + 200
     
        pygame.draw.line(screen, BLACK, [x,y], [x+5,y], 5)  
    
    # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()   
    # Limit to 20 frames per second
    clock.tick(20)
    
   

pygame.quit()