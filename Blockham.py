import pygame
import constants
from spritesheet import SpriteSheet

class Blockham(pygame.sprite.Sprite):
    """The player character"""
    
    #Attributes
    #images
    images = []
    posx = 0
    posy = 0
    depth = 0
    #0 is stopped, 2468 DLRU, 9 dig
    status = 0
    
    def __init__(self):
        """ Constructor. Pass in the color of the block,
        and its x and y position. """
     
        # Call the parent class (Sprite) constructor
        pygame.sprite.DirtySprite.__init__(self)
        #load the images        
        
        sprite_sheet = SpriteSheet("blockham.bmp")
        self.images.append(sprite_sheet.get_image(10,10,10,10))        
        self.images.append(sprite_sheet.get_image(10,20,10,10))        
        self.images.append(sprite_sheet.get_image(0,10,10,10))
        self.images.append(sprite_sheet.get_image(20,10,10,10))        
        self.images.append(sprite_sheet.get_image(10,0,10,10))
        self.images.append(sprite_sheet.get_image(0,0,10,10)) 
        # Set our transparent color
        self.image = self.images[2]
        # Fetch the rectangle object that has the dimensions of the image.
        # 
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
        
    def update(self):
        if self.status == 0:
            self.image = self.images[0]
        elif self.status == 2:          
            self.image = self.images[1]
        elif self.status == 4:
            self.image = self.images[2]
        elif self.status == 6:
            self.image = self.images[3] 
        elif self.status == 8:
            self.image = self.images[4]
        elif self.status == 9:
            self.image = self.images[5]           
    
    def start_dig(self):
        self.status = 9        
    
    def finish_dig(self):   
        self.status =0          
        
    def start_left(self):
        if self.posx > 0:
            self.status = 4        
        
    def finish_left(self):
        if self.posx > 0:
            self.posx = self.posx - 1
        self.status = 0    
    
    def start_right(self):
        if self.posx < 9:
            self.status = 6
    
    def finish_right(self):
        if self.posx < 9:
            self.posx = self.posx + 1
        self.status = 0   
        
    def start_up(self):
        if self.posy > 0:
            self.status = 8

    def finish_up(self):
        if self.posy > 0:
            self.posy = self.posy - 1
        self.status = 0
            
    def start_down(self):
        if self.posy < 9:
            self.status = 2
        
    def finish_down(self):
        if self.posy < 10:
            self.posy = self.posy + 1
        self.status = 0  
    
    def getPosX(self):
        return self.posx
    
    def getPosY(self):
        return self.posy

    def getDepth(self):
        return self.depth    

    def setDepth(self,depth):
        self.depth=depth
        
    def getStatus(self):
        return self.status
            
            
     
    
