# Sample Python/Pygame Programs
# Simpson College Computer Science
# http://programarcadegames.com/
# http://simpson.edu/computer-science/
  
import pygame
  
# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
BLUE     = (   0,   0, 255)
 
# Setup
pygame.init()
   
# Set the width and height of the screen [width,height]
size = [100, 100]
screen = pygame.display.set_mode(size)
  
pygame.display.set_caption("My Game")
  
#Loop until the user clicks the close button.
done = False
  
# Used to manage how fast the screen updates
clock = pygame.time.Clock()

#image setup
player_image = pygame.image.load("meanblock.bmp").convert()
player_image.set_colorkey(WHITE)

#sound setup
click_sound = pygame.mixer.Sound("click.wav")

# Hide the mouse cursor
pygame.mouse.set_visible(0)
clicked = False
click_n = 0
# -------- Main Program Loop -----------
while not done:
    # ALL EVENT PROCESSING SHOULD GO BELOW THIS COMMENT
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop        
        elif event.type == pygame.MOUSEBUTTONDOWN:
            click_sound.play()            
            clicked = True
            click_n += 16
            if click_n > 255:
                click_n = 0
    # ALL EVENT PROCESSING SHOULD GO ABOVE THIS COMMENT
 
    # ALL GAME LOGIC SHOULD GO BELOW THIS COMMENT
 
    # Call draw stick figure function
    pos = pygame.mouse.get_pos()
    x = pos[0]
    y = pos[1]
 
    # ALL GAME LOGIC SHOULD GO ABOVE THIS COMMENT
        
    # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
      
    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill((click_n,0,0))
    screen.blit(player_image, [x, y])
     
    # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
      
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
  
    # Limit to 20 frames per second
    clock.tick(60)
      
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()