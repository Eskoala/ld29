class Column():
    """The stacks of blocks we're digging into"""
    
    #Attributes
    #images
    posx = 0
    posy = 0
    depth = 0
    
    def __init__(self,x,y,d):
        self.posx=x
        self.posy=y
        self.depth=d
    
    def getPosX(self):
        return self.posx

    def getPosY(self):
        return self.posy

    def getDepth(self):
        return self.depth