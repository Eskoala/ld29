"""
Global constants
"""

# Colors
BLACK    = (   0,   0,   0) 
WHITE    = ( 255, 255, 255) 
MAGENTA  = (   255,   0, 255)


# Screen dimensions
SCREEN_WIDTH  = 220
SCREEN_HEIGHT = 220