import pygame

class Pillar(pygame.sprite.Sprite):
    """The stacks of blocks we're digging into"""
    
    #Attributes
    #images
    posx = 0
    posy = 0
    depth = 0
    hardness = 0
    color = (255,255,255)
    treasure=False
    treasureName ="No"
    treasuredepth = -1
    treasureFound = False
    
    def __init__(self,x,y,d):
        self.posx=x
        self.posy=y
        self.depth=d
        self.updateColor()
    
    def getPosX(self):
        return self.posx

    def getPosY(self):
        return self.posy

    def getDepth(self):
        return self.depth
    
    def getColor(self):
        return self.color

    def dig(self):
        if self.treasureFound == False:    
            self.depth += 1
            if 15 < self.depth:
                self.depth = 15
            self.updateColor()        
            print (self.treasuredepth)
            if True == self.treasure:
                if self.depth == self.treasuredepth:
                    name = self.treasureName
                    self.findTreasure()
                    return name
            return "No"
        else:
            return "No"
        
    def updateColor(self):
        if self.depth == 0:
            k = 255
        else:
            k = 255 - (self.depth *16 -1)
        if self.depth <=13:                       
            self.color = (k,k*4/5,k*3/4)
        elif self.depth == 14:
            self.color = (48,0,0)
        elif self.depth == 15:
            self.color = (90,0,0)
    
    def hasTreasure(self):
        return self.treasure

    def getTreasureName(self):
        return self.treasureName
    
    def setTreasure(self, name, depth):
        self.treasure=True
        self.treasureName = name
        self.treasuredepth = depth
    
    def findTreasure(self):
        self.treasure = False
        self.treasureFound = True
        
    def getTreasureFound(self):
        return self.treasureFound
        